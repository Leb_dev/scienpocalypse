package dev.cyberhatter.scienpocalypse.GameClasses;

import java.util.Random;

public class Building {
    private final String[] BuildingTypes =
    {
        //Residential
        "Hut",
        "Shack",
        "Cottage",
        "Small House",
        "House",
        "Hotel",
        "Skyscraper",
        //Foodstuffs
        "Farm",
        "Grocery Store",
        "Supermarket",
        "Food Warehouse",
        //Ammuntion
        "Hunter's Hut",
        "Gun Shop",
        "Armory Warehouse",
        "Bank",
        "Police Station",
        "Military Base",
        //Industial
        "Dump",
        "Electronics Store",
        "Blacksmith's House",
        "Tinker's",
        "Car Workshop",
        "Repairs Workshop",
        "Mine",
        "Construction Site",
        "Electrostation",
        "Factory",
        //Transport
        //"Pier",
        "Parking",
        "Helipad",
        "Gas Station",
        //"Shipyard",
        //Science
        "Research Institute",
        "Laboratory",
        "School",
        "Library",
        //Medical
        "Healer's Cabin",
        "Pharmacy",
        "Clinic",
        "Hospital",
        //Random
        "Ruins",
        "Theatre",
        "Park",
        "Amusement Park",
        "Museum",
        "Stadium",
        "Swimming Pool",
        "Tower",
        "Offices",
        "Bunker",
        //Clothing
        "Tailor's Shop",
        "Shopping Center",
        "Clothing Store"
    };
    final int[][] BuildingSizes = 
    {
        //Residential
        { 1,   1  },
        { 2,   1  },
        { 2,   2  },
        { 3,   3  },
        { 5,   2  },
        { 3,   2  },
        { 4,   4  },
        //Foodstuffs
        { 2,   2  },
        { 2,   2  },
        { 3,   2  },
        { 4,   3  },
        //Ammunition
        { 1,   1  },
        { 2,   1  },
        { 3,   3  },
        { 2,   2  },
        { 2,   2  },
        { 4,   4  },
        //Industrial
        { 3,   3  },
        { 3,   2  },
        { 1,   1  },
        { 1,   1  },
        { 2,   2  },
        { 2,   2  },
        { 2,   2  },
        { 3,   3  },
        { 4,   4  },
        { 4,   4  },
        //Transport
        //{ 1,   1  },
        { 3,   2  },
        { 2,   2  },
        { 2,   2  },
        //{ 1,   1  },
        //Science
        { 3,   2  },
        { 2,   2  },
        { 3,   3  },
        { 3,   1  },
        //Medical
        { 1,   1  },
        { 2,   1  },
        { 3,   3  },
        { 4,   4  },
        //Random
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
        //Clothing
        { 1,   1  },
        { 1,   1  },
        { 1,   1  },
    };            
    final float[][] BuildingProbabilities =
    {
        //Residential
        { 12,   5,    3,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 4,    5,    10,   10,   9,    5,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    3,    5,    9,    10,   9.5f, 7,    1,    1,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    3,    7,    8,    6,    4,    2,    1,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    3,    6,    7,    7,    5,    3,    2    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    2,    2,    2    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    2,    4,    5    },
        //Foodstuffs
        { 5,    10,   15,   15,   17,   15,   6,    1,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    4,    8,    14,   13,   10,   8,    5,    3,    1,    0.5f, 0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    2,    3,    3,    4,    4,    4,    4.5f, 4.5f, 5    },
        { 0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    2,    2,    2,    2,    2,    1    },
        //Ammunition
        { 4,    10,   12,   12,   3,    2,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    3,    5,    4,    2,    2,    2.5f, 3,    3    },
        { 0,    0,    0,    0,    1,    1,    2,    2,    2,    2,    2,    2,    1,    1,    1,    1    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    2,    2,    1,    2,    2    },
        { 0,    0,    0,    0,    0,    0,    1,    1,    2,    3,    3,    3,    4,    4,    3.5f, 3    },
        { 0,    0,    0,    0,    0.5f, 1,    1.5f, 2,    2.5f, 3,    3,    1.5f, 1,    0.5f, 0.5f, 0    },
        //Industrial
        { 23,   19,   8,    5,    1,    1,    1,    1,    1,    1,    1,    1,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    2,    2,    3,    3,    3.5f, 4    },
        { 0,    0,    4,    6,    6,    5,    3,    1,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    3,    6,    6,    5,    3,    1,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    1,    3,    3,    3,    3,    3,    4,    3.5f, 4,    4    },
        { 0,    0,    2,    2,    2,    2,    3,    3,    3,    2,    2,    2,    2,    1,    1,    1    },
        { 23,   19,   8,    4,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    1,    3,    3,    3,    2,    2,    2,    2,    1,    1    },
        { 0,    0,    0,    0,    2,    4,    6,    8,    6,    5,    4,    2,    2,    2,    1,    1    },
        { 0,    0,    0,    0,    0,    0,    1,    1,    4,    3,    3,    2,    2,    2,    1,    1    },
        //Transport
        { 0,    0,    5,    5,    8,    10,   8,    5,    2,    2,    1,    1,    2,    1,    1,    0    },
        { 0,    0,    0,    0,    0,    2,    3,    3,    3,    3,    2,    2,    2,    2,    2.5f, 3    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    2,    2,    2.5f, 2.5f, 2    },
        { 4,    10,   10,   10,   12,   11,   9,    8,    8,    9,    7,    8,    8,    8,    9,    10.5f},
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,    1,    2,    2.5f, 3    },
        //Science
        { 0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    2,    3,    3,    3,    3    },
        { 0,    0,    0,    0,    1,    1,    2,    3,    3,    3,    4,    4,    4,    3,    2.5f, 2    },
        { 0,    0,    0,    3,    4,    4,    5,    6,    5.5f, 4,    4,    3,    3,    3,    2,    2    },
        { 0,    0,    0,    0,    1,    3,    4,    5,    5,    4,    4,    4,    3,    2.5f, 2,    2    },
        //Medical
        { 2,    3,    5,    6,    6,    3,    2,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    2,    3,    3,    3,    3,    4,    4,    4,    4,    4,    4.5f },
        { 0,    0,    0,    0,    0,    0,    1,    2,    3,    3,    4,    4,    4,    4.5f, 4,    4    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    2,    3,    3,    3,    3.5f, 3    },
        //Random
        { 15,   10,   5,    3,    2,    2,    2,    1,    1,    1,    1,    1,    1,    1,    1,    1    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,    1,    2,    2,    1    },
        { 0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,    2,    2,    2.5f, 3,    3    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    0,    1,    1,    1    },
        { 0,    0,    0,    0,    0,    0,    2,    1,    1,    2,    2,    2,    2,    2.5f, 2.5f, 3    },
        { 0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,    2,    2,    2.5f, 2.5f, 3    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    2,    2,    2.5f, 2.5f, 3    },
        { 6,    5,    3,    1,    0,    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    0,    1,    3,    3,    4,    3,    3,    2    },
        { 2,    4,    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,    1,    1,    2,    2    },
        //Clothing
        { 0,    0,    4,    6,    5,    3,    1,    0,    0,    0,    0,    0,    0,    0,    0,    0    },
        { 0,    0,    0,    0,    0,    0,    0,    0,    1,    1,    1,    1,    1,    1.5f, 2,    3    },
        { 0,    0,    0,    0,    0,    0,    0,    2,    2,    2,    3,    3,    3,    3,    2.5f, 3    },
    };

    public boolean turned;
    private int BuildingType = 0;
    private int x;
    private int y;
    private int xSize;
    private int ySize;
    public String BuildingTypeString;
    public int rotation;

    Building(int localitySize, Random random) {
        float seed = new Random().nextFloat() * 100;
        for (int i = 0; i < BuildingTypes.length; i++) {
            seed -= BuildingProbabilities[i][localitySize];
            if (seed < 0) {
                BuildingType = i;
                BuildingTypeString = BuildingTypes[i];
                break;
            }
        }
        rotation = random.nextInt(4);
        turned = rotation == 1 || rotation == 3;
        if (!turned) {
            xSize = BuildingSizes[BuildingType][0];
            ySize = BuildingSizes[BuildingType][1];
        }
        else {
            xSize = BuildingSizes[BuildingType][1];
            ySize = BuildingSizes[BuildingType][0];
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    void setX(int x) {
        this.x = x;
    }

    void setY(int y) {
        this.y = y;
    }

    public int getXSize() {
        return xSize;
    }

    public int getYSize() {
        return ySize;
    }
}