package dev.cyberhatter.scienpocalypse.GameClasses;

public class Protagonist {
    //Основные характеристики [0..100 %]
    public float Health;                    //Здоровье
    public float Satiety;                   //Сытость
    public float Water;                     //Вода
    public float RadioactiveContamination;  //Радиоактивное заражение

    //Дополнительные характеристики
    public float MaxWeight;                 //Переносимый вес [кг]
    public int BackpackCapacity;            //Количество слотов в рюкзаке [количество слотов]
    public int AmmoCapacity;                //Вместимость подсумка с патронами [количество слотов]
    public float RadiationResistance;       //Сопротивляемость радиации [0..100 %]
    public float WoundsResistance;          //Устойчивость к ранениям [0..100 %]
    public float SatietyDecreasePerDay;     //Уменьшение сытости в день
    public float WaterDecreasePerDay;       //Уменьшение воды за день
    public float OnFootSpeed;               //Пешая скорость [км/ч]

    public float DaysTravelling;            //Дней путешествия (Всего)
    public int DD;                        //Дней путешествия
    public int HH;                        //Часов путешествия
    public int MM;                        //Минут путешествия

    public Protagonist() {
        Health = 100f;
        Satiety = 100f;
        Water = 100f;
        RadioactiveContamination = 0f;

        MaxWeight = 40f;
        BackpackCapacity = 4;
        AmmoCapacity = 2;
        RadiationResistance = 2f;
        WoundsResistance = 0.4f;
        DaysTravelling = 0.0f;
        SatietyDecreasePerDay = 15.0f;
        WaterDecreasePerDay = 25.0f;
        OnFootSpeed = 5f;
    }

    public void addDaysTravelling(float addition) {
        DaysTravelling += addition;
        float timeTraveled = DaysTravelling;
        DD = (int)Math.floor(timeTraveled);
        timeTraveled = (timeTraveled - DD) * 24;
        HH = (int)Math.floor(timeTraveled);
        timeTraveled = (timeTraveled - HH) * 60;
        MM = (int)Math.floor(timeTraveled);
    }
}