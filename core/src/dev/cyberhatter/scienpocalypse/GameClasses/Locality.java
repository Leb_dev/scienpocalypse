package dev.cyberhatter.scienpocalypse.GameClasses;

import com.badlogic.gdx.utils.Array;

import java.util.Random;

import static dev.cyberhatter.scienpocalypse.GameClasses.Cells.*;

public class Locality {
    //Количество зданий на местности
    private final String[] LocalitySizeTypes =
            {
                    "Wasteland",              //0 Пустошь
                    "UninhabitedArea",        //1 Нежилая территория
                    "Commune",                //2 Коммуна
                    "Steading",               //3 Хутор
                    "Settlement",             //4 Поселение
                    "Village",                //5 Деревня
                    "Perish",                 //6 Волость
                    "SemiUrbanSettlement",    //7 Полугородской Посёлок
                    "UrbanTypeSettlement",    //8 Посёлок Городского Типа
                    "SmallDistrict",          //9 Микрорайон
                    "District",               //10 Район
                    "SmallTown",              //11 Маленький Город
                    "Town",                   //12 Город
                    "City",                   //13 Большой Город
                    "Megapolis",              //14 Мегаполис
                    "Capital"                 //15 Столица
            };
    //km
    private final float[] MinimumDistances =
            {
                    0.3f, 0.5f, 0.8f, 1, 2, 3, 5, 7, 10, 13, 16, 20, 50, 100, 250, 1000
            };
    private final float[] MaximumDistances =
            {
                    6, 9, 12, 15, 18, 21, 23, 25, 30, 35, 40, 60, 100, 300, 750, 7500
            };
    //0..16
    private final int[][] AmountOfBuildingsProbability =
            {
                    {75, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {55, 33, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 50, 30, 10, 0},
                    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 30, 30}
            };
    //-1 0 +1 +2 +3
    private final int[] BigRoadLocalitySizeProbabilities =
            {
                    1, 44, 43, 10, 3
            };
    //+1 0 -1 -2 -3
    private final int[] SmallRoadLocalitySizeProbabilities =
            {
                    2, 57, 35, 5, 1
            };
    //-2 -1 0 +1 +2
    private final int[] WaterLocalitySizeProbabilities =
            {
                    1, 20, 40, 36, 3
            };
    //-2 -1 0 +1 +2
    private final int[] RailwayLocalitySizeProbabilities =
            {
                    3, 36, 40, 20, 1
            };
    private final static int ROAD_TYPE_BIG = 0;
    private final static int ROAD_TYPE_SMALL = 1;
    private final static int ROAD_TYPE_WATER = 2;

    private int LocalitySize;
    private int MapSize;
    private int AmountOfBuildings;

    private String LocalitySizeString;

    private boolean BigRoad;
    private boolean SmallRoad;
    private boolean Water;

    private float MinimumDistance;
    private float MaximumDistance;

    private Locality BigRoadLocality;
    private Locality SmallRoadLocality;
    private Locality WaterLocality;

    public Array<Cell> exitPoints = null;

    public Array<Building> Buildings = null;

    public int EarthType = 0;

    public int[][] Map = null;

    public class Cell {

        public int x;
        public int y;

        Cell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        double Distance(Cell cell) {
            return Math.sqrt(
                    ((x - cell.x) * (x - cell.x)) +
                            ((y - cell.y) * (y - cell.y))
            );
        }
    }

    public int getMapSize() {
        return MapSize;
    }

    public Locality getLocality(int type) {
        switch (type) {
            case ROAD_TYPE_BIG:
                return BigRoadLocality;
            case ROAD_TYPE_SMALL:
                return SmallRoadLocality;
            case ROAD_TYPE_WATER:
                return WaterLocality;
            default:
                return null;
        }
    }

    public Locality(int size) {
        LocalitySize = size;
        MapSize = 8 + LocalitySize;
        LocalitySizeString = LocalitySizeTypes[size];

        Random random = new Random();

        //Decide on amount of buildings
        int seed = random.nextInt(100) + 1;
        int sum = 0;
        for (int i = 0; i < AmountOfBuildingsProbability[size].length; i++) {
            sum += AmountOfBuildingsProbability[size][i];
            if (seed <= sum) {
                AmountOfBuildings = i;
                break;
            }
        }

        //Decide on the roads available
        seed = random.nextInt(100) + 1;
        if (seed <= 10 + LocalitySize * 5) {
            BigRoad = false;
            SmallRoad = true;
        } else {
            seed -= 10 + LocalitySize * 5;
            if (seed <= 5) {
                BigRoad = true;
                SmallRoad = false;
            } else {
                BigRoad = true;
                SmallRoad = true;
            }
        }

        //Decide on the water available
        seed = random.nextInt(100) + 1;
        Water = seed <= 5 + LocalitySize * 6;
    }

    public boolean hasMap() {
        return Map != null;
    }

    public void generateMap(int maxEarthTypes) {
        Random random = new Random();

        Buildings = new Array<Building>();

        EarthType = random.nextInt(maxEarthTypes);

        for (int i = 0; i < AmountOfBuildings; i++) {
            Buildings.add(new Building(LocalitySize, random));
        }

        boolean generated;
        do {
            generated = true;

            //Заполнение ничем
            Map = new int[MapSize][MapSize];
            for (int i = 0; i < MapSize; i++) {
                for (int j = 0; j < MapSize; j++) {
                    Map[i][j] = CELL_NOTHING;
                }
            }

            //Добавление воды
            if (Water) {
                int pounds = random.nextInt(4) + 1;
                for (int i = 0; i < pounds; i++) {
                    Cell cell = findFreeBorderCell(random);
                    Map[cell.x][cell.y] = CELL_WATER;
                    //Озеро
                    int waterCells = random.nextInt(MapSize * MapSize * (5 - pounds) / 16) + MapSize;
                    int currentX = cell.x;
                    int currentY = cell.y;
                    do {
                        if (random.nextBoolean()) {
                            if (random.nextBoolean()) {
                                if (currentX + 1 < MapSize) {
                                    currentX++;
                                }
                            } else {
                                if (currentX - 1 >= 0) {
                                    currentX--;
                                }
                            }
                        } else {
                            if (random.nextBoolean()) {
                                if (currentY + 1 < MapSize) {
                                    currentY++;
                                }
                            } else {
                                if (currentY - 1 >= 0) {
                                    currentY--;
                                }
                            }
                        }
                        if (Map[currentX][currentY] == CELL_NOTHING) {
                            Map[currentX][currentY] = CELL_WATER;
                            waterCells--;
                        }
                    } while (waterCells > 0);
                }

                //TODO
                //Детализация воды
                for (int i = 0; i < MapSize; i++) {
                    for (int j = 0; j < MapSize; j++) {
                        if (isWater(Map[i][j])) {
                            boolean[] surroundings = checkWaterAround(i, j);
                            if (surroundings[0] && surroundings[1] && surroundings[2] && !surroundings[3]) {
                                Map[i][j] = CELL_WATER_BANK_VERTICAL_LEFT;
                                continue;
                            }
                            if (surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_BANK_VERTICAL_RIGHT;
                                continue;
                            }
                            if (surroundings[0] && surroundings[1] && !surroundings[2] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_BANK_HORIZONTAL_BOTTOM;
                                continue;
                            }
                            if (!surroundings[0] && surroundings[1] && surroundings[2] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_BANK_HORIZONTAL_TOP;
                                continue;
                            }
                            if (!surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_CORNER_TOP_RIGHT;
                                continue;
                            }
                            if (surroundings[0] && !surroundings[1] && !surroundings[2] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_CORNER_BOTTOM_RIGHT;
                                continue;
                            }
                            if (surroundings[0] && surroundings[1] && !surroundings[2]) {
                                Map[i][j] = CELL_WATER_CORNER_BOTTOM_LEFT;
                                continue;
                            }
                            if (!surroundings[0] && surroundings[1] && surroundings[2]) {
                                Map[i][j] = CELL_WATER_CORNER_TOP_LEFT;
                                continue;
                            }
                            if (!surroundings[0] && !surroundings[1] && surroundings[2]) {
                                Map[i][j] = CELL_WATER_END_TOP;
                                continue;
                            }
                            if (!surroundings[0] && !surroundings[1] && surroundings[3]) {
                                Map[i][j] = CELL_WATER_END_RIGHT;
                                continue;
                            }
                            if (surroundings[0] && !surroundings[1] && !surroundings[2]) {
                                Map[i][j] = CELL_WATER_END_BOTTOM;
                                continue;
                            }
                            if (!surroundings[0] && surroundings[1] && !surroundings[3]) {
                                Map[i][j] = CELL_WATER_END_LEFT;
                                continue;
                            }
                            if (surroundings[0] && !surroundings[1]) {
                                Map[i][j] = CELL_WATER_NARROW_VERTICAL;
                                continue;
                            }
                            if (!surroundings[0] && surroundings[1]) {
                                Map[i][j] = CELL_WATER_NARROW_HORIZONTAL;
                                //continue;
                            }
                        }
                    }
                }
            }

            final int maxReseeds = 250;
            int reseedCounter = -1;
            //Располагаем здания
            for (Building building : Buildings) {
                int x = -1;
                int y = -1;
                boolean reseed;
                do {
                    reseedCounter++;
                    if (reseedCounter > maxReseeds) {
                        generated = false;
                        break;
                    }
                    reseed = false;
                    x = random.nextInt(MapSize - 1 - building.getXSize()) + 1;
                    y = random.nextInt(MapSize - 1 - building.getYSize()) + 1;
                    //Проверка под зданием и вокруг
                    for (int i = x; i < x + building.getXSize(); i++) {
                        for (int j = y; j < y + building.getYSize(); j++) {
                            if (Map[i][j] != CELL_NOTHING) {
                                reseed = true;
                                break;
                            }
                        }
                        if (reseed) {
                            break;
                        }
                    }
                    if (reseed) {
                        continue;
                    }

                    //Проверка вокруг здания
                    boolean allWater = true;
                    for (int i = x; i < x + building.getXSize(); i++) {
                        if (
                                (!isWater(Map[i][y - 1]) || !isWater(Map[i][y + building.getYSize()])) &&
                                        allWater
                                ) {
                            allWater = false;
                        }
                        if (Map[i][y - 1] == CELL_BUILDING || Map[i][y + building.getYSize()] == CELL_BUILDING) {
                            reseed = true;
                            break;
                        }
                    }
                    if (reseed) {
                        continue;
                    }
                    for (int j = y; j < y + building.getYSize(); j++) {
                        if (
                                (!isWater(Map[x - 1][j]) || !isWater(Map[x + building.getXSize()][j])) &&
                                        allWater
                                ) {
                            allWater = false;
                        }
                        if (Map[x - 1][j] == CELL_BUILDING || Map[x + building.getXSize()][j] == CELL_BUILDING) {
                            reseed = true;
                            break;
                        }
                    }
                    if (allWater) {
                        reseed = true;
                    }
                } while (reseed);
                if (!generated) {
                    break;
                }
                if (x != -1 && y != -1) {
                    building.setX(x);
                    building.setY(y);
                    for (int i = x; i < x + building.getXSize(); i++) {
                        for (int j = y; j < y + building.getYSize(); j++) {
                            Map[i][j] = CELL_BUILDING;
                        }
                    }
                }
            }
            if (!generated) {
                continue;
            }

            //Дороги
            Array<Cell> pointsToConnect = new Array<Cell>();
            Array<Cell> buildingExits = new Array<Cell>();
            exitPoints = new Array<Cell>();

            boolean reseed;

            //Creating roads
            int roads = random.nextInt(4) + 1;
            for (int i = 0; i < roads; i++) {
                reseedCounter = 0;
                Cell smallRoadStartingCell;
                Cell smallRoadEndingCell;
                do {
                    reseed = false;
                    smallRoadStartingCell = findFreeBorderCell(random);
                    smallRoadEndingCell = findFreeBorderCell(random);
                    if (smallRoadStartingCell.Distance(smallRoadEndingCell) < 7) {
                        reseed = true;
                        reseedCounter++;
                        if (reseedCounter > maxReseeds) {
                            generated = false;
                            break;
                        }
                    }
                } while (reseed);
                if (!generated) {
                    break;
                }
                exitPoints.add(smallRoadStartingCell, smallRoadEndingCell);
                Cell cell = fillPath(smallRoadStartingCell, smallRoadEndingCell, random);
                if (cell == null) {
                    generated = false;
                    break;
                } else {
                    pointsToConnect.add(cell);
                }
            }
            if (!generated) {
                continue;
            }

            for (int i = 0; i < pointsToConnect.size; i++) {
                for (int j = i + 1; j < pointsToConnect.size; j++) {
                    if (fillPath(pointsToConnect.get(i), pointsToConnect.get(j), random) == null) {
                        generated = false;
                        break;
                    }
                }
                if (!generated) {
                    break;
                }
            }

            //Соединяем здания
            for (Building building : Buildings) {
                boolean connected = false;
                int x = building.getX();
                int y = building.getY();
                int xSize = building.getXSize();
                int ySize = building.getYSize();
                //Checking y +- 1
                for (int i = x; i < x + xSize; i++) {
                    if (Cells.isRoad(Map[i][y - 1]) || Cells.isRoad(Map[i][y + ySize])) {
                        if (Cells.isRoad(Map[i][y - 1])) {
                            buildingExits.add(new Cell(i, y - 1));
                        } else {
                            buildingExits.add(new Cell(i, y + ySize));
                        }
                        connected = true;
                        break;
                    }
                }
                if (connected) {
                    continue;
                }
                //Checking x +- 1
                for (int j = y; j < y + ySize; j++) {
                    if (Cells.isRoad(Map[x - 1][j]) || Cells.isRoad(Map[x + xSize][j])) {
                        if (Cells.isRoad(Map[x - 1][j])) {
                            buildingExits.add(new Cell(x - 1, j));
                        } else {
                            buildingExits.add(new Cell(x + xSize, j));
                        }
                        connected = true;
                        break;
                    }
                }
                if (!connected) {
                    Cell newExit;
                    do {
                        if (random.nextBoolean()) {
                            //x
                            if (random.nextBoolean()) {
                                //left
                                newExit = new Cell(x - 1, random.nextInt(building.getYSize()) + building.getY());
                            } else {
                                //right
                                newExit = new Cell(x + xSize, random.nextInt(building.getYSize()) + building.getY());
                            }
                        } else {
                            //y
                            if (random.nextBoolean()) {
                                //up
                                newExit = new Cell(random.nextInt(building.getXSize()) + building.getX(), y + ySize);
                            } else {
                                //down
                                newExit = new Cell(random.nextInt(building.getXSize()) + building.getX(), y - 1);
                            }
                        }
                    } while (isWater(Map[newExit.x][newExit.y]));
                    if (
                            fillPath(
                                    newExit,
                                    pointsToConnect.get(random.nextInt(pointsToConnect.size)),
                                    random
                            ) == null) {
                        generated = false;
                        break;
                    } else {
                        buildingExits.add(newExit);
                    }
                }
            }
            if (!generated) {
                continue;
            }

            //TODO
            //Детализация дорог
            for (int i = 0; i < MapSize; i++) {
                for (int j = 0; j < MapSize; j++) {
                    if (isSimpleRoad(Map[i][j])) {
                        boolean[] surroundings = checkRoadsAround(i, j, buildingExits);
                        if (surroundings[0] && surroundings[1] && surroundings[2] && !surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_T_CROSS_NO_LEFT;
                            continue;
                        }
                        if (surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_T_CROSS_NO_RIGHT;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_T_CROSS_NO_DOWN;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_T_CROSS_NO_UP;
                            continue;
                        }
                        if (!surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_TURN_DOWN_LEFT;
                            continue;
                        }
                        if (surroundings[0] && !surroundings[1] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_TURN_LEFT_UP;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1] && !surroundings[2]) {
                            Map[i][j] = CELL_ROAD_SMALL_TURN_UP_RIGHT;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[2]) {
                            Map[i][j] = CELL_ROAD_SMALL_TURN_RIGHT_DOWN;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1]) {
                            Map[i][j] = CELL_ROAD_SMALL_X_CROSS;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[3] ||
                                !surroundings[0] && surroundings[1] ||
                                !surroundings[0] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_ROAD_SMALL_HORIZONTAL;
                        }
                    }
                }
            }
            //Детализация мостов
            for (int i = 0; i < MapSize; i++) {
                for (int j = 0; j < MapSize; j++) {
                    if (isBridge(Map[i][j])) {
                        boolean[] surroundings = checkRoadsAround(i, j, buildingExits);
                        if (surroundings[0] && surroundings[1] && surroundings[2] && !surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_T_CROSS_NO_LEFT;
                            continue;
                        }
                        if (surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_T_CROSS_NO_RIGHT;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_T_CROSS_NO_DOWN;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_T_CROSS_NO_UP;
                            continue;
                        }
                        if (!surroundings[0] && !surroundings[1] && surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_TURN_DOWN_LEFT;
                            continue;
                        }
                        if (surroundings[0] && !surroundings[1] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_TURN_LEFT_UP;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1] && !surroundings[2]) {
                            Map[i][j] = CELL_BRIDGE_TURN_UP_RIGHT;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[2]) {
                            Map[i][j] = CELL_BRIDGE_TURN_RIGHT_DOWN;
                            continue;
                        }
                        if (surroundings[0] && surroundings[1]) {
                            Map[i][j] = CELL_BRIDGE_X_CROSS;
                            continue;
                        }
                        if (!surroundings[0] && surroundings[1] && surroundings[3] ||
                                !surroundings[0] && surroundings[1] ||
                                !surroundings[0] && !surroundings[2] && surroundings[3]) {
                            Map[i][j] = CELL_BRIDGE_HORIZONTAL;
                        }
                    }
                }
            }
        } while (!generated);
    }

    private boolean[] checkWaterAround(int i, int j) {
        boolean[] surroundings = {false, false, false, false};
        if (j == MapSize - 1) {
            surroundings[0] = true;
        } else {
            if (isWater(Map[i][j + 1])) {
                surroundings[0] = true;
            }
        }
        if (i == MapSize - 1) {
            surroundings[1] = true;
        } else {
            if (isWater(Map[i + 1][j])) {
                surroundings[1] = true;
            }
        }
        if (j == 0) {
            surroundings[2] = true;
        } else {
            if (isWater(Map[i][j - 1])) {
                surroundings[2] = true;
            }
        }
        if (i == 0) {
            surroundings[3] = true;
        } else {
            if (isWater(Map[i - 1][j])) {
                surroundings[3] = true;
            }
        }
        return surroundings;
    }

    private boolean[] checkRoadsAround(int i, int j, Array<Cell> buildingExits) {
        boolean[] surroundings = {false, false, false, false};
        boolean exitPoint = belongsTo(i, j, exitPoints);
        boolean buildingExit = belongsTo(i, j, buildingExits);
        if (j != MapSize - 1) {
            if (isRoad(Map[i][j + 1]) || (Map[i][j + 1] == CELL_BUILDING && buildingExit)) {
                surroundings[0] = true;
            }
        } else {
            if (exitPoint) {
                surroundings[0] = true;
            }
        }
        if (i != MapSize - 1) {
            if (isRoad(Map[i + 1][j]) || (Map[i + 1][j] == CELL_BUILDING && buildingExit)) {
                surroundings[1] = true;
            }
        } else {
            if (exitPoint) {
                surroundings[1] = true;
            }
        }
        if (j != 0) {
            if (isRoad(Map[i][j - 1]) || (Map[i][j - 1] == CELL_BUILDING && buildingExit)) {
                surroundings[2] = true;
            }
        } else {
            if (exitPoint) {
                surroundings[2] = true;
            }
        }
        if (i != 0) {
            if (isRoad(Map[i - 1][j]) || (Map[i - 1][j] == CELL_BUILDING && buildingExit)) {
                surroundings[3] = true;
            }
        } else {
            if (exitPoint) {
                surroundings[3] = true;
            }
        }
        return surroundings;
    }

    private boolean belongsTo(int x, int y, Array<Cell> cells) {
        for (Cell cell : cells) {
            if (cell.x == x && cell.y == y) {
                return true;
            }
        }
        return false;
    }

    private Cell fillPath(Cell startingCell, Cell endingCell, Random random) {
        //Copy the Map
        int[][] passMap = Map.clone();
        for (int i = 0; i < Map.length; i++) {
            passMap[i] = Map[i].clone();
        }
        for (int i = 0; i < MapSize; i++) {
            for (int j = 0; j < MapSize; j++) {
                switch (passMap[i][j]) {
                    case CELL_NOTHING:
                        if (i == 0 || j == 0 ||
                                i == MapSize - 1 || j == MapSize - 1) {
                            passMap[i][j] = 15;
                        } else {
                            passMap[i][j] = 5;
                        }
                        break;
                    case CELL_BUILDING:
                        passMap[i][j] = 800;
                        break;
                    case CELL_BRIDGE:
                        passMap[i][j] = 2;
                        break;
                    default:
                        if (isWater(passMap[i][j])) {
                            passMap[i][j] = 80;
                            break;
                        }
                        if (isRoad(passMap[i][j])) {
                            passMap[i][j] = 1;
                            break;
                        }
                }
            }
        }
        Array<Cell> path = findPath(startingCell, endingCell, passMap, random);
        if (path == null) {
            return null;
        }
        for (Cell cell : path) {
            if (Map[cell.x][cell.y] == CELL_NOTHING) {
                Map[cell.x][cell.y] = Cells.CELL_ROAD_SMALL;
            } else {
                if (isWater(Map[cell.x][cell.y])) {
                    Map[cell.x][cell.y] = Cells.CELL_BRIDGE;
                }
            }
        }
        return path.get(random.nextInt(path.size));
    }

    private Array<Cell> findPath(Cell startingCell, Cell endingCell, int[][] passMap, Random
            random) {
        int[][] waveMap = new int[MapSize][MapSize];
        boolean[][] visitedMap = new boolean[MapSize][MapSize];
        for (int i = 0; i < MapSize; i++) {
            for (int j = 0; j < MapSize; j++) {
                waveMap[i][j] = -1;
                visitedMap[i][j] = false;
            }
        }
        waveMap[startingCell.x][startingCell.y] = passMap[startingCell.x][startingCell.y];
        visitedMap[startingCell.x][startingCell.y] = true;
        Array<Cell> pathHeads = new Array<Cell>();
        pathHeads.add(startingCell);
        boolean hasEndingCell;
        do {
            //Finding the minimum value "head"
            if (pathHeads.size == 0) {
                break;
            }
            Cell cell = pathHeads.first();
            int minValue = waveMap[cell.x][cell.y];
            int minIndex = 0;
            for (int i = 1; i < pathHeads.size; i++) {
                cell = pathHeads.get(i);
                if (waveMap[cell.x][cell.y] < minValue) {
                    minValue = waveMap[cell.x][cell.y];
                    minIndex = i;
                }
            }
            //Saving the head and removing it
            cell = pathHeads.get(minIndex);
            pathHeads.removeIndex(minIndex);
            //Adding mutations
            //Up
            Cell mutatedCell = new Cell(cell.x, cell.y + 1);
            if (mutatedCell.y < MapSize) {
                addMutation(mutatedCell, cell, waveMap, passMap, visitedMap, pathHeads);
            }
            //Right
            mutatedCell = new Cell(cell.x + 1, cell.y);
            if (mutatedCell.x < MapSize) {
                addMutation(mutatedCell, cell, waveMap, passMap, visitedMap, pathHeads);
            }
            //Down
            mutatedCell = new Cell(cell.x, cell.y - 1);
            if (mutatedCell.y >= 0) {
                addMutation(mutatedCell, cell, waveMap, passMap, visitedMap, pathHeads);
            }
            //Left
            mutatedCell = new Cell(cell.x - 1, cell.y);
            if (mutatedCell.x >= 0) {
                addMutation(mutatedCell, cell, waveMap, passMap, visitedMap, pathHeads);
            }
            hasEndingCell = false;
            for (Cell cellToCheck : pathHeads) {
                if (cellToCheck.x == endingCell.x && cellToCheck.y == endingCell.y) {
                    hasEndingCell = true;
                    break;
                }
            }
        } while (!hasEndingCell);
        //Creating the path
        Array<Cell> path = new Array<Cell>();
        path.add(endingCell);
        boolean hasStartingCell;
        do {
            Cell cell = path.get(path.size - 1);
            int cellValue = waveMap[cell.x][cell.y];
            Array<Cell> cellsToAdd = new Array<Cell>();
            //Up
            Cell mutatedCell = new Cell(cell.x, cell.y + 1);
            if (mutatedCell.y < MapSize) {
                cellValue = checkAndAddCell(mutatedCell, waveMap, cellValue, cellsToAdd);
            }
            //Right
            mutatedCell = new Cell(cell.x + 1, cell.y);
            if (mutatedCell.x < MapSize) {
                cellValue = checkAndAddCell(mutatedCell, waveMap, cellValue, cellsToAdd);
            }
            //Down
            mutatedCell = new Cell(cell.x, cell.y - 1);
            if (mutatedCell.y >= 0) {
                cellValue = checkAndAddCell(mutatedCell, waveMap, cellValue, cellsToAdd);
            }
            //Left
            mutatedCell = new Cell(cell.x - 1, cell.y);
            if (mutatedCell.x >= 0) {
                checkAndAddCell(mutatedCell, waveMap, cellValue, cellsToAdd);
            }
            if (cellsToAdd.size == 0) {
                return null;
            }
            path.add(cellsToAdd.get(random.nextInt(cellsToAdd.size)));
            hasStartingCell = false;
            for (Cell cellToCheck : path) {
                if (cellToCheck.x == startingCell.x && cellToCheck.y == startingCell.y) {
                    hasStartingCell = true;
                    break;
                }
            }
        } while (!hasStartingCell);
        return path;
    }

    private int checkAndAddCell(Cell mutatedCell, int[][] waveMap, int cellValue, Array<
            Cell> cellsToAdd) {
        if (waveMap[mutatedCell.x][mutatedCell.y] != -1) {
            if (cellValue > waveMap[mutatedCell.x][mutatedCell.y]) {
                cellsToAdd.clear();
                cellsToAdd.add(mutatedCell);
                return waveMap[mutatedCell.x][mutatedCell.y];
            } else {
                if (cellValue == waveMap[mutatedCell.x][mutatedCell.y]) {
                    cellsToAdd.add(mutatedCell);
                }
            }
        }
        return cellValue;
    }

    private void addMutation(
            Cell newCell,
            Cell oldCell,
            int[][] waveMap,
            int[][] passMap,
            boolean[][] visitedMap,
            Array<Cell> pathHeads
    ) {
        if (!visitedMap[newCell.x][newCell.y]) {
            waveMap[newCell.x][newCell.y] = waveMap[oldCell.x][oldCell.y] +
                    passMap[newCell.x][newCell.y];
            visitedMap[newCell.x][newCell.y] = true;
            pathHeads.add(newCell);
        }
    }

    private Cell findFreeBorderCell(Random random) {
        boolean reseed;
        int x, y;
        do {
            reseed = false;
            if (random.nextBoolean()) {
                x = random.nextBoolean() ? MapSize - 1 : 0;
                y = random.nextInt(MapSize);
            } else {
                x = random.nextInt(MapSize);
                y = random.nextBoolean() ? MapSize - 1 : 0;
            }
            if (Map[x][y] != CELL_NOTHING) {
                reseed = true;
            }
        } while (reseed);
        return new Cell(x, y);
    }

    public void setOuterLocalities() {
        if (BigRoad) {
            BigRoadLocality = setLocality(BigRoadLocalitySizeProbabilities, -1, true);
        }
        if (SmallRoad) {
            SmallRoadLocality = setLocality(SmallRoadLocalitySizeProbabilities, 1, false);
        }
        if (Water) {
            WaterLocality = setLocality(WaterLocalitySizeProbabilities, -2, true);
        }
    }

    private Locality setLocality(int[] localitySizeProbabilities, int defaultSizeChange,
                                 boolean inc) {
        int newLocalitySize;
        boolean reseed;
        Random random = new Random();

        do {
            newLocalitySize = LocalitySize;
            reseed = false;
            int seed = random.nextInt(100) + 1;
            int sizeChange = defaultSizeChange;
            for (int localitySizeProbability : localitySizeProbabilities) {
                if (seed <= localitySizeProbability) {
                    newLocalitySize += sizeChange;
                    if (newLocalitySize < 0 || newLocalitySize >= LocalitySizeTypes.length) {
                        reseed = true;
                    }
                    break;
                } else {
                    seed -= localitySizeProbability;
                    if (inc) {
                        sizeChange++;
                    } else {
                        sizeChange--;
                    }
                }
            }
        } while (reseed);

        Locality locality = new Locality(newLocalitySize);

        float minimumDistance =
                MinimumDistances[newLocalitySize] +
                        (MaximumDistances[newLocalitySize] - MinimumDistances[newLocalitySize]) *
                                random.nextFloat();
        float maximumDistance =
                MinimumDistances[newLocalitySize] +
                        (MaximumDistances[newLocalitySize] - MinimumDistances[newLocalitySize]) *
                                random.nextFloat();
        if (minimumDistance > maximumDistance) {
            float Buf = minimumDistance;
            minimumDistance = maximumDistance;
            maximumDistance = Buf;
        }
        locality.MinimumDistance = minimumDistance;
        locality.MaximumDistance = maximumDistance;

        return locality;
    }
}