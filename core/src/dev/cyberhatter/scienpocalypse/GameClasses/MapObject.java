package dev.cyberhatter.scienpocalypse.GameClasses;

public class MapObject {
    public float x;
    public float y;
    public float angle;

    public MapObject(float x, float y, float angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;
    }
}
