package dev.cyberhatter.scienpocalypse.GameClasses;

public final class Cells {
    //Базовые клетки
    public final static int CELL_NOTHING = 0;
    public final static int CELL_WATER = 1;
    public final static int CELL_BUILDING = 2;
    public final static int CELL_ROAD_SMALL = 4;
    public final static int CELL_BRIDGE = 5;

    //Прориосванные клетки
    //Вода
    public final static int CELL_WATER_BANK_VERTICAL_LEFT = 20;
    public final static int CELL_WATER_BANK_VERTICAL_RIGHT = 21;
    public final static int CELL_WATER_BANK_HORIZONTAL_TOP = 22;
    public final static int CELL_WATER_BANK_HORIZONTAL_BOTTOM = 23;
    public final static int CELL_WATER_CORNER_TOP_LEFT = 24;
    public final static int CELL_WATER_CORNER_TOP_RIGHT = 25;
    public final static int CELL_WATER_CORNER_BOTTOM_RIGHT = 26;
    public final static int CELL_WATER_CORNER_BOTTOM_LEFT = 27;
    public final static int CELL_WATER_END_TOP = 28;
    public final static int CELL_WATER_END_RIGHT = 29;
    public final static int CELL_WATER_END_BOTTOM = 30;
    public final static int CELL_WATER_END_LEFT = 31;
    public final static int CELL_WATER_NARROW_VERTICAL = 32;
    public final static int CELL_WATER_NARROW_HORIZONTAL = 33;
    //Маленькая дорога
    public final static int CELL_ROAD_SMALL_HORIZONTAL = 40;
    public final static int CELL_ROAD_SMALL_TURN_UP_RIGHT = 41;
    public final static int CELL_ROAD_SMALL_TURN_RIGHT_DOWN = 42;
    public final static int CELL_ROAD_SMALL_TURN_DOWN_LEFT = 43;
    public final static int CELL_ROAD_SMALL_TURN_LEFT_UP = 44;
    public final static int CELL_ROAD_SMALL_T_CROSS_NO_UP = 45;
    public final static int CELL_ROAD_SMALL_T_CROSS_NO_RIGHT = 46;
    public final static int CELL_ROAD_SMALL_T_CROSS_NO_DOWN = 47;
    public final static int CELL_ROAD_SMALL_T_CROSS_NO_LEFT = 48;
    public final static int CELL_ROAD_SMALL_X_CROSS = 49;
    //Мост
    public final static int CELL_BRIDGE_HORIZONTAL = 60;
    public final static int CELL_BRIDGE_TURN_UP_RIGHT = 61;
    public final static int CELL_BRIDGE_TURN_RIGHT_DOWN = 62;
    public final static int CELL_BRIDGE_TURN_DOWN_LEFT = 63;
    public final static int CELL_BRIDGE_TURN_LEFT_UP = 64;
    public final static int CELL_BRIDGE_T_CROSS_NO_UP = 65;
    public final static int CELL_BRIDGE_T_CROSS_NO_RIGHT = 66;
    public final static int CELL_BRIDGE_T_CROSS_NO_DOWN = 67;
    public final static int CELL_BRIDGE_T_CROSS_NO_LEFT = 68;
    public final static int CELL_BRIDGE_X_CROSS = 69;

    public static boolean isRoad(int cellType) {
        return  cellType == CELL_ROAD_SMALL ||
                cellType == CELL_BRIDGE ||
                (cellType >= CELL_ROAD_SMALL_HORIZONTAL && cellType <= CELL_ROAD_SMALL_X_CROSS) ||
                (cellType >= CELL_BRIDGE_HORIZONTAL && cellType <= CELL_BRIDGE_X_CROSS);
    }

    public static boolean isBridge(int cellType) {
        return  cellType == CELL_BRIDGE ||
                (cellType >= CELL_BRIDGE_HORIZONTAL && cellType <= CELL_BRIDGE_X_CROSS);
    }

    public static boolean isSimpleRoad(int cellType) {
        return  cellType == CELL_ROAD_SMALL ||
                (cellType >= CELL_ROAD_SMALL_HORIZONTAL && cellType <= CELL_ROAD_SMALL_X_CROSS);
    }

    public static boolean isWater(int cellType) {
        return cellType == CELL_WATER ||
                (cellType >= CELL_WATER_BANK_VERTICAL_LEFT && cellType <= CELL_WATER_NARROW_HORIZONTAL);
    }
}
