package dev.cyberhatter.scienpocalypse.GameClasses;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import dev.cyberhatter.scienpocalypse.GameScreen;

public class Joystick extends Actor {

    GameScreen gameScreen;

    private Sprite knob;
    private Sprite circle;

    private Vector2 offsetPosition = new Vector2(0, 0);
    private Vector2 originPosition = new Vector2(0, 0);

    private boolean touched = false;

    //private int touchIndex;

    float angle;

    public Joystick(GameScreen gameScreen, Sprite circle, Sprite knob) {
        this.gameScreen = gameScreen;
        this.circle = circle;
        this.knob = knob;

        setWidth(gameScreen.stage.getWidth() * 2 / 5);
        setHeight(gameScreen.stage.getHeight() * 2 / 3);

        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                touched = true;
                originPosition.set(x, y);
                offsetPosition.set(0, 0);
                return true;
            }

            //при перетаскивании
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                offsetPosition.set(x - originPosition.x, y - originPosition.y);
            }

            //убираем палец с экрана
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                touched = false;
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (touched) {
            circle.setPosition(originPosition.x - circle.getWidth() / 2, originPosition.y - circle.getHeight() / 2);
            circle.setSize(256,256);
            circle.draw(batch);
            offsetPosition.clamp(0, circle.getWidth() * 1 / 3);
            knob.setPosition(originPosition.x - knob.getWidth() / 2 + offsetPosition.x,
                    originPosition.y - knob.getHeight() / 2 + offsetPosition.y);
            knob.setSize(256,256);
            knob.setScale(0.5f);
            knob.draw(batch);
        }
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        //Процедура проверки. Если точка в прямоугольнике актёра, возвращаем актёра.
        return x > 0 && x < getWidth() && y > 0 && y < getHeight() ? this : null;
        //return null;
    }

    /*
    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched, float x, float y, int index) {
        if (!this.touched && touched) {
            setPosition(x, y);
            this.touchIndex = index;
        }
        this.touched = touched;
    }

    public int getTouchIndex() {
        return touchIndex;
    }*/
}


/*
public class WalkingControl extends Actor{

	//размер джоя
	public static  float SIZE = 4f;
	//размер движущейся части (khob)
	public static  float CSIZE = 3f;

	public static  float CIRCLERADIUS = 1.5f;
	public static float  CONTRLRADIUS = 3F;
	//public static float Coefficient = 1F;

	//угол для определения направления
	float angle;
	//public static int Opacity = 1;
	 World world;

	//координаты отклонения khob
	protected Vector2 offsetPosition = new Vector2();

	protected Vector2 position = new Vector2();
	protected Rectangle bounds = new Rectangle();

	public WalkingControl(Vector2 pos, World world){

	}

	public void withControl(float x, float y){

		//точка касания относительно центра джойстика
		float calcX = x/world.ppuX -SIZE/2;
		float calcY = y/world.ppuY -SIZE/2;

		//определяем лежит ли точка касания в окружности джойстика
		if(((calcX*calcX + calcY* calcY)<=WalkingControl.CONTRLRADIUS*WalkingControl.CONTRLRADIUS)
				){

			world.resetSelected();

			//пределяем угол касания
			double angle = Math.atan(calcY/calcX)*180/Math.PI;

			//угол будет в диапозоне [-90;90]. Удобнее работать, если он в диапозоне [0;360]
			//поэтому пошаманим немного
			if(angle>0 &&calcY<0)
					angle+=180;
			if(angle <0)
				if(calcX<0)
					angle=180+angle;
				else
					angle+=360;

			//в зависимости от угла указываем направление, куда двигать игрока
			if(angle>40 && angle<140)
				((Player)world.selectedActor).upPressed();

			if(angle>220 && angle<320)
				((Player)world.selectedActor).downPressed();


			if(angle>130 && angle<230)
				((Player)world.selectedActor).leftPressed();

			if(angle<50 || angle>310)
				((Player)world.selectedActor).rightPressed();


			//двигаем игрока
			((Player)world.selectedActor).processInput();


			angle = (float)(angle*Math.PI/180);
			getOffsetPosition().x = (float)((calcX*calcX + calcY* calcY>1F)? Math.cos(angle)*0.75F: calcX);
			getOffsetPosition().y = (float)((calcX*calcX + calcY* calcY>1F)? Math.sin(angle)*0.75F: calcY);

		}
		else{

			world.resetSelected();
			getOffsetPosition().x = 0;
			getOffsetPosition().y = 0;
		}

	}
}
*/