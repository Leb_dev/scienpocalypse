package dev.cyberhatter.scienpocalypse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Random;

import dev.cyberhatter.scienpocalypse.GameClasses.Building;
import dev.cyberhatter.scienpocalypse.GameClasses.Joystick;
import dev.cyberhatter.scienpocalypse.GameClasses.Locality;
import dev.cyberhatter.scienpocalypse.GameClasses.MapObject;
import dev.cyberhatter.scienpocalypse.GameClasses.Protagonist;

import static dev.cyberhatter.scienpocalypse.GameClasses.Cells.*;

//GLOBAL TODO
//Desktop build: gradlew desktop:dist
//D:\ProGg\Android\Projects\Scienpocalypse\desktop\build\libs
/*
+ Refresh building tables
+ Create player
+ Implement travelling
*/

public class GameScreen implements Screen {

    private final int PPU = 32;
    public final Stage stage;

    private enum GameEvents {
        None,
        GameOver
    }

    private BitmapFont gameFont;

    private int WorldSize;
    private Scienpocalypse game;
    private Protagonist protagonist;
    private Locality CurrentLocality;
    private Locality NextLocality;
    private GameEvents GameEvent = GameEvents.None;

    private TextureAtlas textureAtlas;
    private Array<Sprite> earth;

    private Sprite building;
    private HashMap<String, Sprite> buildingsHashMap;
    private Sprite joystickCircle;
    private Sprite joystickKnob;
    private Sprite water;
    private Sprite waterBankVertical;
    private Sprite waterBankHorizontal;
    private Sprite waterCornerTopLeft;
    private Sprite waterCornerTopRight;
    private Sprite waterEndLeft;
    private Sprite waterEndTop;
    private Sprite waterNarrowVertical;
    private Sprite waterNarrowHorizontal;
    private Sprite road;
    private Sprite roadTurn;
    private Sprite roadTCross;
    private Sprite roadXCross;
    private Sprite bridge;
    private Sprite bridgeTurn;
    private Sprite bridgeTCross;
    private Sprite bridgeXCross;
    private Sprite playerSprite;
    private Sprite lens;
    private Sprite buttonUp;
    private Sprite buttonDown;
    private Sprite arrows;

    private OrthographicCamera camera;

    private MapObject player;

    private Random random = new Random();

    private boolean viewMode = false;

    GameScreen(Scienpocalypse game) {
        this.game = game;

        gameFont = new BitmapFont();
        gameFont.getData().setScale(0.4f);

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        loadTextures();

        addUI(stage);

        protagonist = new Protagonist();

        setRandomCurrentLocality(0);

        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(w, h);
    }

    private void addUI(Stage stage) {
        stage.addActor(new Joystick(this, joystickCircle, joystickKnob));
        Button changeModeButton = new Button(new SpriteDrawable(buttonUp), new SpriteDrawable(buttonDown));
        changeModeButton.setSize(64, 64);
        changeModeButton.setPosition(
                stage.getWidth() - changeModeButton.getWidth(),
                stage.getHeight() - changeModeButton.getHeight()
        );
        final Image changeModeImage = new Image(lens);
        changeModeImage.setOrigin(32, 32);
        changeModeImage.setScale(0.8f);
        changeModeButton.add(changeModeImage);
        changeModeButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                viewMode = !viewMode;
                if (viewMode) {
                    changeModeImage.setDrawable(new SpriteDrawable(arrows));
                } else {
                    changeModeImage.setDrawable(new SpriteDrawable(lens));
                }
            }
        });
        stage.addActor(changeModeButton);
    }

    private void loadTextures() {
        textureAtlas = new TextureAtlas("Sprites.txt");

        earth = new Array<Sprite>(textureAtlas.createSprites("Earth"));

        building = textureAtlas.createSprite("Building");
        buildingsHashMap = new HashMap<String, Sprite>();

        water = textureAtlas.createSprite("Water");
        waterBankVertical = textureAtlas.createSprite("Water_Bank_Vertical");
        waterBankHorizontal = textureAtlas.createSprite("Water_Bank_Horizontal");
        waterCornerTopLeft = textureAtlas.createSprite("Water_Corner_Top_Left");
        waterCornerTopRight = textureAtlas.createSprite("Water_Corner_Top_Right");
        waterEndLeft = textureAtlas.createSprite("Water_End_Left");
        waterEndTop = textureAtlas.createSprite("Water_End_Top");
        waterNarrowVertical = textureAtlas.createSprite("Water_Narrow_Vertical");
        waterNarrowHorizontal = textureAtlas.createSprite("Water_Narrow_Horizontal");

        road = textureAtlas.createSprite("Road");
        roadTurn = textureAtlas.createSprite("Road_Turn");
        roadTCross = textureAtlas.createSprite("Road_T_Cross");
        roadXCross = textureAtlas.createSprite("Road_X_Cross");

        bridge = textureAtlas.createSprite("Bridge");
        bridgeTurn = textureAtlas.createSprite("Bridge_Turn");
        bridgeTCross = textureAtlas.createSprite("Bridge_T_Cross");
        bridgeXCross = textureAtlas.createSprite("Bridge_X_Cross");

        joystickCircle = textureAtlas.createSprite("Joystick_Circle");
        joystickKnob = textureAtlas.createSprite("Joystick_Knob");

        playerSprite = textureAtlas.createSprite("Player");

        buttonUp = textureAtlas.createSprite("Button_Up");
        buttonDown = textureAtlas.createSprite("Button_Down");

        lens = textureAtlas.createSprite("Lens");

        arrows = textureAtlas.createSprite("Arrows");
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        handleInput();

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        drawMap();
        //drawUI();
        game.batch.end();

        stage.act();
        stage.draw();
    }

    private void handleInput() {
        //OrthographicCamera camera = (OrthographicCamera) stage.getCamera();
        if (!viewMode) {
            camera.position.set(player.x, player.y, camera.position.z);
        } else {
            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                camera.translate(-3, 0, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                camera.translate(3, 0, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                camera.translate(0, -3, 0);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                camera.translate(0, 3, 0);
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
            camera.zoom += 0.015f;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            camera.zoom -= 0.015f;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            setRandomCurrentLocality(random.nextInt(16));
        }

        if (camera.viewportWidth > camera.viewportHeight) {
            camera.zoom = MathUtils.clamp(camera.zoom, 0.005f, WorldSize / camera.viewportWidth);
        } else {
            camera.zoom = MathUtils.clamp(camera.zoom, 0.005f, WorldSize / camera.viewportHeight);
        }

        /*if (!joystick.isTouched()) {
            boolean touched = false;
            float x = 0, y = 0;
            int index = 0;
            for (int i = 0; i <= 5; i++) {
                if (Gdx.input.isTouched(i)) {
                    x = Gdx.input.getX(i);
                    y = Gdx.graphics.getHeight() - Gdx.input.getY();
                    if (x < Gdx.graphics.getWidth() / 5 * 2 &&
                            y < Gdx.graphics.getHeight() / 3 * 2) {
                        touched = true;
                        index = i;
                        break;
                    }
                }
            }
            joystick.setTouched(touched, x, y, index);
        } else {
            if (!Gdx.input.isTouched(joystick.getTouchIndex())) {
                joystick.setTouched(false, 0, 0, 0);
            }
        }*/

        float effectiveViewportWidth = camera.viewportWidth * camera.zoom / 4f;
        float effectiveViewportHeight = camera.viewportHeight * camera.zoom / 4f;

        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth, WorldSize - effectiveViewportWidth);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight, WorldSize - effectiveViewportHeight);
    }

    private void setRandomCurrentLocality(int i) {
        int size = 0;
        do {
            CurrentLocality = new Locality(i);
            WorldSize = PPU * CurrentLocality.getMapSize();
            //CurrentLocality.setOuterLocalities();
            CurrentLocality.generateMap(earth.size);
            size = CurrentLocality.exitPoints.size;
        } while (size == 0);
        Locality.Cell cell = CurrentLocality.exitPoints.get(random.nextInt(size));
        player = new MapObject(cell.x * PPU, cell.y * PPU, 0);
    }

    private void drawMap() {
        if (CurrentLocality != null) {
            Sprite earth_sprite = earth.get(CurrentLocality.EarthType);
            earth_sprite.setSize(64, 64);
            for (int i = 0; i < Math.ceil(WorldSize / 64f); i++) {
                for (int j = 0; j < Math.ceil(WorldSize / 64f); j++) {
                    earth_sprite.setPosition(i * 64, j * 64);
                    earth_sprite.draw(game.batch);
                }
            }
            if (CurrentLocality.hasMap()) {
                for (int i = 0; i < CurrentLocality.getMapSize(); i++) {
                    for (int j = 0; j < CurrentLocality.getMapSize(); j++) {
                        switch (CurrentLocality.Map[i][j]) {
                            //Water
                            case CELL_WATER:
                                drawTile(water, i, j, 0);
                                break;
                            case CELL_WATER_BANK_VERTICAL_LEFT:
                                drawTile(waterBankVertical, i, j, 0);
                                break;
                            case CELL_WATER_BANK_VERTICAL_RIGHT:
                                drawTile(waterBankVertical, i, j, 180);
                                break;
                            case CELL_WATER_BANK_HORIZONTAL_TOP:
                                drawTile(waterBankHorizontal, i, j, 0);
                                break;
                            case CELL_WATER_BANK_HORIZONTAL_BOTTOM:
                                drawTile(waterBankHorizontal, i, j, 180);
                                break;
                            case CELL_WATER_CORNER_TOP_LEFT:
                                drawTile(waterCornerTopLeft, i, j, 0);
                                break;
                            case CELL_WATER_CORNER_BOTTOM_RIGHT:
                                drawTile(waterCornerTopLeft, i, j, 180);
                                break;
                            case CELL_WATER_CORNER_TOP_RIGHT:
                                drawTile(waterCornerTopRight, i, j, 0);
                                break;
                            case CELL_WATER_CORNER_BOTTOM_LEFT:
                                drawTile(waterCornerTopRight, i, j, 180);
                                break;
                            case CELL_WATER_END_TOP:
                                drawTile(waterEndTop, i, j, 0);
                                break;
                            case CELL_WATER_END_BOTTOM:
                                drawTile(waterEndTop, i, j, 180);
                                break;
                            case CELL_WATER_END_LEFT:
                                drawTile(waterEndLeft, i, j, 0);
                                break;
                            case CELL_WATER_END_RIGHT:
                                drawTile(waterEndLeft, i, j, 180);
                                break;
                            case CELL_WATER_NARROW_VERTICAL:
                                drawTile(waterNarrowVertical, i, j, 0);
                                break;
                            case CELL_WATER_NARROW_HORIZONTAL:
                                drawTile(waterNarrowHorizontal, i, j, 0);
                                break;

                            //Roads    
                            case CELL_ROAD_SMALL:
                                drawTile(road, i, j, 0);
                                break;
                            case CELL_ROAD_SMALL_HORIZONTAL:
                                drawTile(road, i, j, 90);
                                break;
                            case CELL_ROAD_SMALL_TURN_UP_RIGHT:
                                drawTile(roadTurn, i, j, 0);
                                break;
                            case CELL_ROAD_SMALL_TURN_RIGHT_DOWN:
                                drawTile(roadTurn, i, j, 90);
                                break;
                            case CELL_ROAD_SMALL_TURN_DOWN_LEFT:
                                drawTile(roadTurn, i, j, 180);
                                break;
                            case CELL_ROAD_SMALL_TURN_LEFT_UP:
                                drawTile(roadTurn, i, j, 270);
                                break;
                            case CELL_ROAD_SMALL_T_CROSS_NO_RIGHT:
                                drawTile(roadTCross, i, j, 0);
                                break;
                            case CELL_ROAD_SMALL_T_CROSS_NO_DOWN:
                                drawTile(roadTCross, i, j, 90);
                                break;
                            case CELL_ROAD_SMALL_T_CROSS_NO_LEFT:
                                drawTile(roadTCross, i, j, 180);
                                break;
                            case CELL_ROAD_SMALL_T_CROSS_NO_UP:
                                drawTile(roadTCross, i, j, 270);
                                break;
                            case CELL_ROAD_SMALL_X_CROSS:
                                drawTile(roadXCross, i, j, 0);
                                break;

                            //bridges    
                            case CELL_BRIDGE:
                                drawTile(water, i, j, 0);
                                drawTile(bridge, i, j, 0);
                                break;
                            case CELL_BRIDGE_HORIZONTAL:
                                drawTile(water, i, j, 0);
                                drawTile(bridge, i, j, 90);
                                break;
                            case CELL_BRIDGE_TURN_UP_RIGHT:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTurn, i, j, 0);
                                break;
                            case CELL_BRIDGE_TURN_RIGHT_DOWN:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTurn, i, j, 90);
                                break;
                            case CELL_BRIDGE_TURN_DOWN_LEFT:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTurn, i, j, 180);
                                break;
                            case CELL_BRIDGE_TURN_LEFT_UP:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTurn, i, j, 270);
                                break;
                            case CELL_BRIDGE_T_CROSS_NO_RIGHT:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTCross, i, j, 0);
                                break;
                            case CELL_BRIDGE_T_CROSS_NO_DOWN:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTCross, i, j, 90);
                                break;
                            case CELL_BRIDGE_T_CROSS_NO_LEFT:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTCross, i, j, 180);
                                break;
                            case CELL_BRIDGE_T_CROSS_NO_UP:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeTCross, i, j, 270);
                                break;
                            case CELL_BRIDGE_X_CROSS:
                                drawTile(water, i, j, 0);
                                drawTile(bridgeXCross, i, j, 0);
                                break;
                        }
                    }
                }
            }
            //Draw building
            String buildingTypeString;
            Sprite buildingSprite;
            for (Building building : CurrentLocality.Buildings) {
                buildingTypeString = "Building_" + building.BuildingTypeString;
                if (!buildingsHashMap.containsKey(buildingTypeString)) {
                    buildingSprite = this.building;
                } else {
                    buildingSprite = buildingsHashMap.get(buildingTypeString);
                }
                //Rotate around the centre
                if (building.getXSize() == building.getYSize() || !building.turned) {
                    buildingSprite.setPosition(building.getX() * PPU, building.getY() * PPU);
                    buildingSprite.setSize(building.getXSize() * PPU, building.getYSize() * PPU);
                    buildingSprite.setOrigin(building.getXSize() * PPU / 2, building.getYSize() * PPU / 2);
                    buildingSprite.setRotation(-90 * building.rotation);
                    buildingSprite.draw(game.batch);
                } else {
                    //rotate around the "square" center
                    int min = Math.min(building.getXSize(), building.getYSize());
                    if (building.rotation == 3) {
                        buildingSprite.setPosition(building.getX() * PPU, building.getY() * PPU);
                        buildingSprite.setSize(building.getYSize() * PPU, building.getXSize() * PPU);
                        buildingSprite.setOrigin(min * PPU / 2, min * PPU / 2);
                        buildingSprite.setRotation(-270);
                        buildingSprite.draw(game.batch);
                    } else {
                        buildingSprite.setPosition(
                                building.getX() * PPU,
                                (building.getY() + (building.getYSize() - building.getXSize())) * PPU
                        );
                        buildingSprite.setSize(building.getYSize() * PPU, building.getXSize() * PPU);
                        buildingSprite.setOrigin(min * PPU / 2, min * PPU / 2);
                        buildingSprite.setRotation(-90);
                        buildingSprite.draw(game.batch);
                    }
                }
                final GlyphLayout layout = new GlyphLayout(gameFont, building.BuildingTypeString + building.rotation);
                final float fontX = building.getX() * PPU + (building.getXSize() * PPU - layout.width) / 2;
                final float fontY = building.getY() * PPU + (building.getYSize() * PPU + layout.height) / 2;
                gameFont.draw(game.batch, layout, fontX, fontY);
            }
            //Draw player
            playerSprite.setPosition(player.x, player.y);
            playerSprite.setSize(PPU, PPU);
            //playerSprite.setRotation(player.angle);
            playerSprite.draw(game.batch);
        }
    }

    private void drawTile(Sprite sprite, int i, int j, int rotation) {
        sprite.setPosition(i * PPU, j * PPU);
        sprite.setOrigin(PPU / 2, PPU / 2);
        sprite.setSize(PPU, PPU);
        sprite.setRotation(-rotation);
        sprite.draw(game.batch);
    }

   /* private void drawUI() {
        if (protagonist != null) {
            game.font.draw(game.batch, "Health: " + protagonist.Health, camera.position.x, camera.position.y + 100);
            game.font.draw(game.batch, "Satiety: " + protagonist.Satiety, camera.position.x, camera.position.y + 75);
            game.font.draw(game.batch, "Water: " + protagonist.Water, camera.position.x, camera.position.y + 50);
            game.font.draw(game.batch, "Radioactive contamination: " + protagonist.RadioactiveContamination, camera.position.x, camera.position.y + 25);
            game.font.draw(
                    game.batch,
                    "Days traveled: " +
                            protagonist.DD + "D " +
                            protagonist.HH + "H " +
                            protagonist.MM + "M",
                    game.defaultWidth - 200,
                    25
            );
        }
    }*/

    @Override
    public void resize(int width, int height) {
        //OrthographicCamera camera = (OrthographicCamera) stage.getCamera();
        if (width > height) {
            camera.viewportHeight = camera.viewportWidth * height / width;
        } else {
            camera.viewportWidth = camera.viewportHeight * width / height;
        }
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        textureAtlas.dispose();
        gameFont.dispose();
    }

    public void travel(int roadType) {
        //0 - Маленькая
        //1 - Большая
        //2 - Железная
        //3 - Вода
        if (GameEvent != GameEvents.GameOver) {
            if (CurrentLocality.getLocality(roadType) != null) {
                NextLocality = CurrentLocality.getLocality(roadType);
            } else {
                return;
            }
            /*
            float daysPassed = Random.Range(NextLocality.minimumDistance, NextLocality.maximumDistance) / protagonist.OnFootSpeed / 24f;
            DecreaseSatietyAndWater(daysPassed);
            protagonist.DaysTravelling += daysPassed;
            CurrentLocality = NextLocality;
            CurrentLocality.SetOuterLocalites();
            gameObject.GetComponent<GameUIControl>().LocalityChanged(CurrentLocality);
            TODO
            */
        }
    }

    /*
    TODO
    private void DecreaseSatietyAndWater(float daysPassed)
    {
        float satietyDecrement = daysPassed * protagonist.SatietyDecreasePerDay;
        float waterDecrement = daysPassed * protagonist.WaterDecreasePerDay;
        if (protagonist.Satiety >= satietyDecrement)
        {
            protagonist.Satiety -= satietyDecrement;
        }
        else
        {
            satietyDecrement -= protagonist.Satiety;
            protagonist.Satiety = 0;
            float HealthDecrement = satietyDecrement * 2f;
            if (protagonist.Health > HealthDecrement)
            {
                protagonist.Health -= HealthDecrement;
            }
            else
            {
                protagonist.Health = 0;
                gameObject.GetComponent<GameUIControl>().GameEvent = GameUIControl.GameEvents.GameOver;
                gameObject.GetComponent<GameUIControl>().GameOverCause = GameUIControl.GameOverCauses.Starvation;
            }
        }
        if (protagonist.Water >= waterDecrement)
        {
            protagonist.Water -= waterDecrement;
        }
        else
        {
            waterDecrement -= protagonist.Water;
            protagonist.Water = 0;
            float HealthDecrement = waterDecrement * 2f;
            if (protagonist.Health > HealthDecrement)
            {
                protagonist.Health -= HealthDecrement;
            }
            else
            {
                protagonist.Health = 0;
                gameObject.GetComponent<GameUIControl>().GameEvent = GameUIControl.GameEvents.GameOver;
                gameObject.GetComponent<GameUIControl>().GameOverCause = GameUIControl.GameOverCauses.Dehydration;
            }
        }
    }*/
}
