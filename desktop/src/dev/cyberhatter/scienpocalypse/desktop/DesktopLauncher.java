package dev.cyberhatter.scienpocalypse.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import dev.cyberhatter.scienpocalypse.Scienpocalypse;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Scienpocalypse";
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new Scienpocalypse(), config);
	}
}
